package com.valentina.fruteriabit;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.valentina.fruteriabit.daos.FrutaDao;
import com.valentina.fruteriabit.database.AppDatabase;
import com.valentina.fruteriabit.entities.Fruta;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class FrutaTest {
    private FrutaDao frutaDao;
    private AppDatabase appDatabase;

    @Before
    public void createDb(){
        Context context = ApplicationProvider.getApplicationContext();
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        frutaDao = appDatabase.frutaDao();
    }

    @After
    public void closeDb() throws IOException {
        appDatabase.close();
    }

    @Test
    public void findByNameTest() throws Exception {
        Fruta fruta = new Fruta();
        fruta.setId(1);
        fruta.setNombre("naranja");

        frutaDao.insert(fruta);

        Fruta buscada = frutaDao.findByNombre("naranja");

        assertTrue( "No encontrada la naranja que tenía", fruta.getId() == buscada.getId());
        }
    }